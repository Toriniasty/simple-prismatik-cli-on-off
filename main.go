package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("it's either on or off")
		os.Exit(1)
	}
	if os.Args[1] != "on" && os.Args[1] != "off" {
		fmt.Println("it's either on or off")
		os.Exit(1)
	}

	// connect to server
	conn, _ := net.Dial("tcp", "127.0.0.1:3636")
	message, _ := bufio.NewReader(conn).ReadString('\n')
	fmt.Print(message)

	text := "lock"
	fmt.Fprintf(conn, text+"\n")
	message, _ = bufio.NewReader(conn).ReadString('\n')
	fmt.Print(message)

	text = "setstatus:" + os.Args[1]
	fmt.Fprintf(conn, text+"\n")
	message, _ = bufio.NewReader(conn).ReadString('\n')
	fmt.Print(message)

	text = "unlock"
	fmt.Fprintf(conn, text+"\n")
	message, _ = bufio.NewReader(conn).ReadString('\n')
	fmt.Print(message)
}
